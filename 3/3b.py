from random import random

def prueba(y):
	return (1.0-y)*y/(2.0*y**2.0-2.0*y+1.0)**2.0

n = 10000
total = 0.0

for i in range(n):
	total += prueba(random())

print (total/float(n))