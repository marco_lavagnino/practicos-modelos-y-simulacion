from random import random
from math import e

constante = e**(-3.)

def prueba():
    parcial = 1
    i = 0
    while parcial > constante:
        parcial *= random()
        i += 1
    return i-1

resultados = {}
n = 10000

for i in range(n):
    try:
        this = prueba()
        resultados[this] += 1
    except KeyError:
        resultados[this] = 1

total = 0
for (ues, cantidad) in resultados.items():
    total += ues * cantidad
    
print "E[N] = " + str(total/float(n))

print "-----"

for (ues, cantidad) in resultados.items():
    print "P(N=" + str(ues) + ") = " + str(cantidad/float(n))