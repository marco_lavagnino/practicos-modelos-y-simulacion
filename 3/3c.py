from random import random
from math import e

def prueba(y):
	return e ** -((1/y-1)**2) / y**2

n = 100000
total = 0.0

for i in range(n):
	total += prueba(random())

print (total/float(n))