import random

n = 10000
total = 0.0
for i in range(n):
	total += (1.0 - random.random()**2.0)**(3.0/2.0)

print (total/n)