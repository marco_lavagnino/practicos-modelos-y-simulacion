from random import random
from math import e

n = 100000
total = 0.

for i in range(n):
	total += e**((random()+random())**2)

print (total/float(n))