# MODELOS Y SIMULACION #

Este repositorio contiene el código de respuesta para las guías de modelos y simulación. Si encontrás algún error, hacelo saber! :)

### Estructura del repo ###

Las carpetas en el repo representan cada una de las guías. Dentro de cada carpeta, los archivos siguen la forma <numero\><item\>.py

Para un ver rápido qué ejercicios están resultos, es muy útil el comando `tree .`
